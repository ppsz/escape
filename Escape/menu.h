#pragma once
#include "main.h"

enum MENU_POSITION { MP_START, MP_LOAD, MP_SAVE, MP_EXIT };
enum MENU_STATE { MS_MAIN_MENU, MS_LOAD, MS_SAVE };
struct Supervisor;

struct Menu
{
	int position_;
	int x_;
	int y_;
	int interval_;
	int counter_;
	int menu_state_;
	int *game_state_;
	ALLEGRO_FONT *font_;

	Menu(int *game_state, ALLEGRO_DISPLAY_MODE &disp_data, ALLEGRO_FONT *font);

	void Render();
	void Update();
	void Navigate(int key, float mouse_y_position = 0);
	void Select(Supervisor *supervisor);
};

