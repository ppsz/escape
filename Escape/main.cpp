#include "Main.h"
#include "Player.h"
#include "Enemy.h"
#include "Shadow.h"
#include "Obstacle.h"
#include "Supervisor.h"
#include "level.h"
#include "menu.h"

int main(int argc, char **argv)
{
	//==============================================
	//SHELL VARIABLES
	//==============================================
	bool done = false;
	bool render = false;
	std::array<bool, 6> keys = { false, false, false, false, false, false };
	float game_time = 0;
	int radius;
	int frames = 0;
	int game_FPS = 0;
	int game_state = MAIN_MENU;

	//==============================================
	//PROJECT VARIABLES
	//==============================================
	int number_of_enemies;
	std::ifstream config_file("config.cfg");
	config_file >> number_of_enemies;
	config_file.close();

	Supervisor* supervisor;
	Player* player;
	Shadow* shadow;
	Menu* menu;
	std::vector<Enemy> enemies;

	//==============================================
	//ALLEGRO VARIABLES
	//==============================================
	ALLEGRO_DISPLAY	*display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_FONT *arial_18 = NULL;
	ALLEGRO_FONT *arial_32 = NULL;
	ALLEGRO_FONT *arial_128 = NULL;
	ALLEGRO_SAMPLE *death = NULL;
	ALLEGRO_SAMPLE *zombie_1 = NULL;
	ALLEGRO_SAMPLE *zombie_2 = NULL;
	ALLEGRO_SAMPLE *zombie_3 = NULL;
	ALLEGRO_SAMPLE *zombie_4 = NULL;
	ALLEGRO_SAMPLE *zombie_5 = NULL;
	ALLEGRO_SAMPLE *wind = NULL;

	ALLEGRO_DISPLAY_MODE disp_data;
	ALLEGRO_TRANSFORM camera;

	//==============================================
	//ALLEGRO INIT FUNCTIONS
	//==============================================
	if (!al_init())														//initialize Allegro
		return -1;

	bool window = true;

	al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);
	al_set_new_display_flags(ALLEGRO_OPENGL);
	if(window)
		al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);
	else
		al_set_new_display_flags(ALLEGRO_FULLSCREEN);						//full screen
	al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);
	display = al_create_display(disp_data.width, disp_data.height);		//create our display object
	al_hide_mouse_cursor(display);

	if (!display)														//test display object
		return -1;

	//==============================================
	//ADDON INSTALL
	//==============================================
	al_install_keyboard();
	al_install_mouse();
	al_install_audio();
	al_init_acodec_addon();
	al_init_image_addon();
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_primitives_addon();

	//==============================================
	//PROJECT INIT
	//==============================================
	al_reserve_samples(50);
	al_set_new_bitmap_flags(ALLEGRO_MIN_LINEAR | ALLEGRO_MAG_LINEAR); //filtrowanie tekstur, ewentualne przeniesienie do opcji
	arial_18 = al_load_font("data\\fonts\\arial.ttf", 18, 0);
	arial_32 = al_load_font("data\\fonts\\arial.ttf", 32, 0);
	arial_128 = al_load_font("data\\fonts\\arial.ttf", 32, 0);

	//SPLASH SCREEN
	al_draw_filled_rectangle(0, 0, disp_data.width, disp_data.height, al_map_rgb(0, 0, 0));
	al_draw_text(arial_128, al_map_rgb(255, 255, 255), disp_data.width / 2, disp_data.height / 2, ALLEGRO_ALIGN_CENTRE, "ESCAPE");
	al_flip_display();
	//

	Level level(disp_data);
	level.Load();
	player = new Player(level.spawn_point_);
	shadow = new Shadow(disp_data);
	menu = new Menu(&game_state, disp_data, arial_18);
	supervisor = new Supervisor(player, shadow, menu, &level, enemies, number_of_enemies, game_state, done, disp_data);
	supervisor->ChangeState(MAIN_MENU);

	//==============================================
	//MASK
	//==============================================
	radius = (disp_data.width /2 < disp_data.height / 2 ? disp_data.width / 2 : disp_data.height / 2);

	ALLEGRO_BITMAP* overlay = al_create_bitmap(disp_data.width, disp_data.height);
	al_set_target_bitmap(overlay);
	al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO);// overwrite blender
	al_clear_to_color(al_map_rgba(0, 0, 0, 255));
	al_set_blender(ALLEGRO_DEST_MINUS_SRC, ALLEGRO_ONE, ALLEGRO_ONE);// subtract source values
	al_draw_filled_circle(disp_data.width / 2, disp_data.height / 2, radius * 0.9, al_map_rgba(0, 0, 0,255));
	
	al_set_target_bitmap(al_get_backbuffer(display));
	al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO);// overwrite
	al_clear_to_color(al_map_rgba(0, 0, 0, 0));
	al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);// non-premultiplied alpha

	//==============================================
	//TIMER INIT AND STARTUP
	//==============================================
	event_queue = al_create_event_queue();
	timer = al_create_timer(1.0 / 60);

	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_mouse_event_source());
	al_register_event_source(event_queue, al_get_display_event_source(display));

	al_start_timer(timer);
	game_time = al_current_time();
	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		//==============================================
		//INPUT
		//==============================================
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_LEFT:
				keys[LEFT] = true;
				break;
			case ALLEGRO_KEY_RIGHT:
				keys[RIGHT] = true;
				break;
			case ALLEGRO_KEY_UP:
				keys[UP] = true;
				break;
			case ALLEGRO_KEY_DOWN:
				keys[DOWN] = true;
				break;
			case ALLEGRO_KEY_SPACE:
				keys[CONFIRM] = true;
				break;
			case ALLEGRO_KEY_ENTER:
				keys[CONFIRM] = true;
				break;
			case ALLEGRO_KEY_ESCAPE:
				keys[ESC] = true;
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_UP)
		{
			switch (ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_LEFT:
				keys[LEFT] = false;
				break;
			case ALLEGRO_KEY_RIGHT:
				keys[RIGHT] = false;
				break;
			case ALLEGRO_KEY_UP:
				keys[UP] = false;
				break;
			case ALLEGRO_KEY_DOWN:
				keys[DOWN] = false;
				break;
			case ALLEGRO_KEY_SPACE:
				keys[CONFIRM] = false;
				break;
			case ALLEGRO_KEY_ENTER:
				keys[CONFIRM] = false;
				break;
			case ALLEGRO_KEY_ESCAPE:
				keys[ESC] = false;
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			done = true;
		}

		//==============================================
		//GAME UPDATE
		//==============================================
		else if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			render = true;

			switch (game_state)
			{
			case MAIN_MENU:

				if (keys[UP])
					menu->Navigate(UP);
				else if (keys[DOWN])
					menu->Navigate(DOWN);
				else if (keys[CONFIRM])
					menu->Select(supervisor);
				else

					menu->counter_ = 0;
					//menu->Navigate(MOUSE, mouse_y_coordinate);
				break;
			case PLAY:

				//UPDATE KEYS
				if (keys[ESC])
					supervisor->ChangeState(PAUSE);

				if (keys[UP])
					player->MoveForward();
				else if (keys[DOWN])
					player->MoveBackward();
				else
					player->StopMovement();

				if (keys[LEFT])
					player->RotateLeft();
				else if (keys[RIGHT])
					player->RotateRight();
				else
					player->StopRotation();
				//=====================

				player->Update();
				supervisor->DetectColissions();
				for (auto &enemy : enemies)
					enemy.Update(player, radius);
				break;
			case PAUSE:
				if (keys[UP])
					menu->Navigate(UP);
				else if (keys[DOWN])
					menu->Navigate(DOWN);
				else if (keys[CONFIRM])
					menu->Select(supervisor);
				else
					menu->counter_ = 0;
				break;
			case GAME_OVER:
				if (keys[CONFIRM])
					supervisor->ChangeState(MAIN_MENU);
				break;
			case WIN:
				if (keys[CONFIRM])
					supervisor->ChangeState(MAIN_MENU);
				break;
			}

			//UPDATE FPS
			frames++;
			if (al_current_time() - game_time >= 1)
			{
				game_time = al_current_time();
				game_FPS = frames;
				frames = 0;
			}
			//=====================

		}

		//==============================================
		//RENDER
		//==============================================
		if (render && al_is_event_queue_empty(event_queue))
		{
			al_clear_to_color(al_map_rgb(255, 255, 255));
			render = false;

			//BEGIN PROJECT RENDER
			switch (game_state)
			{
			case MAIN_MENU:
				menu->Render();
				break;
			case PLAY:
				//Moving Camera
				al_identity_transform(&camera);
				al_translate_transform(&camera, std::floorf(disp_data.width / 2 - player->x_), std::floorf(disp_data.height / 2 - player->y_));
				al_use_transform(&camera);
				//
				
				al_draw_filled_rectangle(level.exit_point_[0] - 32, level.exit_point_[1] - 32, level.exit_point_[0] + 32, level.exit_point_[1] + 32, al_map_rgb(100, 255, 100));
				level.Render(player);
				player->Render();
				
				for (auto enemy : enemies)
					enemy.Render();
				shadow->Render(player, level.shadow_obstacles_);

				//Default camera position (to draw GUI)
				al_identity_transform(&camera);
				al_use_transform(&camera);
				//

				//INTERFACE
				al_draw_bitmap(overlay, 0, 0, 0);
				al_draw_textf(arial_32, al_map_rgb(255, 255, 255), 5, disp_data.height - 50, ALLEGRO_ALIGN_LEFT, "+ %i", player->health_);

				break;
			case PAUSE:
				menu->Render();
				break;
			case GAME_OVER:
				menu->Render();
				break;
			case WIN:
				menu->Render();
				break;
			}
			//FLIP BUFFERS
			al_flip_display();
		}
		al_rest(0.001);
	}

	//==============================================
	//DESTROY PROJECT OBJECTS
	//==============================================

	//SHELL OBJECTS
	al_destroy_font(arial_18);
	al_destroy_font(arial_32);
	al_destroy_font(arial_128);
	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_display(display);
	al_destroy_bitmap(overlay);
	al_destroy_sample(death);
	al_destroy_sample(zombie_1);
	al_destroy_sample(zombie_2);
	al_destroy_sample(zombie_3);
	al_destroy_sample(zombie_4);
	al_destroy_sample(zombie_5);
	al_destroy_sample(wind);
	delete supervisor;
	delete player;
	delete shadow;
	return 0;
}