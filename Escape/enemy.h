#pragma once
#include "main.h"

struct Obstacle;
struct Player;

enum AI { AI_INACTIVE, AI_IDLE, AI_CHASE, AI_ATTACK, AI_SPAWN };
enum ANIMATION { AN_WALK = 0, AN_ATTACK = 1, AN_STOP = 1 };

struct Enemy
{
	int id_;
	int unique_id_;
	int state_;

	float x_;
	float y_;
	float degree_;
	float random_degree_;
	float speed_;
	float rotation_speed_;
	float sound_volume_;

	int health_;
	int damage_;
	int hb_x_;
	int hb_y_;

	int max_frame_;
	int cur_frame_;
	int cur_animation_;

	int frame_count_;
	int frame_delay_;
	int frame_width_;
	int frame_height_;

	static ALLEGRO_BITMAP *image_;
	static ALLEGRO_SAMPLE *sample_1_;
	static ALLEGRO_SAMPLE *sample_2_;
	ALLEGRO_SAMPLE_INSTANCE *footstep_1_;
	ALLEGRO_SAMPLE_INSTANCE *footstep_2_;
	static std::array<ALLEGRO_SAMPLE*, 12> zombie_samples_;
	//std::array<ALLEGRO_SAMPLE_INSTANCE*, 5> zombie_sample_instances_;

	Enemy();
	~Enemy();

	void Collide(int &id, float &x, float &y, int &hb_x, int &hb_y, int unique_id = -1);
	void Update(Player *player, int &radius);
	void Render();

	void Spawn(float x, float y, int width, int height);
	void Despawn();
	void Move();
	void Stop();
	void Chase();
	void Idle();
	void Attack(Player *player);
};

