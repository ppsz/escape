#pragma once
#include "Main.h"

enum ROTATION { RT_LEFT = -1, RT_STOP = 0, RT_RIGHT = 1 };
enum MOVEMENT { MV_BACKWARD = -1, MV_STOP = 0, MV_FORWARD = 1 };

struct Player
{
	int id_;
	float x_;
	float y_;
	float degree_;
	float speed_;
	float rotation_speed_;
	float cur_speed_;
	int direction_;
	int rotation_;

	int health_;
	int hb_x_;
	int hb_y_;

	int max_frame_;
	int cur_frame_;
	int weapon_;
	int model_;

	int frame_count_;
	int frame_delay_;
	int frame_width_;
	int frame_height_;

	ALLEGRO_BITMAP *image_;
	ALLEGRO_SAMPLE *sample_1_;
	ALLEGRO_SAMPLE *sample_2_;
	ALLEGRO_SAMPLE_INSTANCE *footstep_1_;
	ALLEGRO_SAMPLE_INSTANCE *footstep_2_;

	Player(std::array<float, 2> spawn_point);
	void Respawn(std::array<float, 2> spawn_point);
	void Spawn(std::array<float, 2> spawn_point);
	~Player();

	void Collide(int &id, float &x, float &y, int &hb_x, int &hb_y);
	void Update();
	void Render();

	void StopMovement();
	void StopRotation();

	void MoveForward();
	void MoveBackward();

	void RotateLeft();
	void RotateRight();
};
