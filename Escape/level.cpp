#include "level.h"
#include "player.h"
#include "obstacle.h"

Level::Level(ALLEGRO_DISPLAY_MODE &disp_data)
{
	w_ = disp_data.width / 2;
	h_ = disp_data.height / 2;
	radius_ = (w_ < h_ ? w_ : h_);

	cur_level_ = 1;
	map_square_size_ = 64;
	obstacle_hb_ = map_square_size_ / 2;

}

bool Level::Load()
{
	obstacles_.clear();
	shadow_obstacles_.clear();
	floor_tiles_.clear();
	vertical_walls_.clear();
	horizontal_walls_.clear();
	top_left_corners_.clear();
	top_right_corners_.clear();
	bot_right_corners_.clear();
	bot_left_corners_.clear();
	bot_t_shapes_.clear();
	top_t_shapes_.clear();
	right_t_shapes_.clear();
	left_t_shapes_.clear();
	bot_ends_.clear();
	left_ends_.clear();
	right_ends_.clear();
	top_ends_.clear();
	columns_.clear();
	x_ = 0;
	y_ = 0;

	std::ifstream map_file("data\\maps\\level" + std::to_string(cur_level_) + ".map");
	
	if (!map_file.is_open())
		return false;
	else
	{
		while (!map_file.eof() && y_ < 64)
		{
			map_file >> map_[x_][y_];
			x_++;
			if (x_ >= 64)
			{
				x_ = 0;
				y_++;
			}
		}
	}
	map_file.close();
	for (int i = 0; i < 64; ++i)
		for (int j = 0; j < 64; ++j)
		{
			int x = i * 64;
			int y = j * 64;

			if (map_[i][j] == 1)
			{
				std::random_device rd;
				std::mt19937 mt(rd());
				std::uniform_int_distribution<int> dist(1, 100);

				int random_number = dist(mt);
				int tile_appearance = 0;

				if (random_number > 1 && random_number < 25)
					tile_appearance = 0;
				else if (random_number > 25 && random_number < 45)
					tile_appearance = 3;
				else if (random_number > 45 && random_number < 55)
					tile_appearance = 1;
				else if (random_number > 55 && random_number < 65)
					tile_appearance = 2;
				else if (random_number > 65 && random_number < 75)
					tile_appearance = 4;
				else if (random_number > 75 && random_number < 85)
					tile_appearance = 5;
				else if (random_number > 90 && random_number < 95)
					tile_appearance = 6;
				else if (random_number > 90 && random_number < 95)
					tile_appearance = 7;
				else if (random_number > 95 && random_number < 100)
					tile_appearance = 8;

				floor_tiles_.push_back(std::array<int, 4>{x, y, lookup_table_[tile_appearance][0], lookup_table_[tile_appearance][1]});
			}
			else if (map_[i][j] == 10)
			{
				obstacles_.push_back(Obstacle(x, y, 32, 64));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 48));
				vertical_walls_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 11)
			{
				obstacles_.push_back(Obstacle(x, y, 64, 32));
				shadow_obstacles_.push_back(Obstacle(x, y, 48, 16));
				horizontal_walls_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 12)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x + 16, y, 32, 16));
				shadow_obstacles_.push_back(Obstacle(x, y + 16, 16, 32));
				top_left_corners_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 13)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x - 16, y, 32, 16));
				shadow_obstacles_.push_back(Obstacle(x, y + 16, 16, 32));
				top_right_corners_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 14)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x - 16, y, 32, 16));
				shadow_obstacles_.push_back(Obstacle(x, y - 16, 16, 32));
				bot_right_corners_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 15)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x + 16, y, 32, 16));
				shadow_obstacles_.push_back(Obstacle(x, y - 16, 16, 32));
				bot_left_corners_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 16)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 16));
				bot_t_shapes_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 17)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 16));
				top_t_shapes_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 18)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 16));
				right_t_shapes_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 19)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 16));
				left_t_shapes_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 20)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 16));
				bot_ends_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 21)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 16));
				top_ends_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 22)
			{
				obstacles_.push_back(Obstacle(x, y));
				shadow_obstacles_.push_back(Obstacle(x, y, 16, 16));
				columns_.push_back(std::array<int, 2>{x, y});
			}
			else if (map_[i][j] == 23)
			{
				//floor_tiles_.push_back(std::array<int, 2>{x, y});
				exit_point_[0] = x;
				exit_point_[1] = y;
			}
			else if (map_[i][j] == 24)
			{
				floor_tiles_.push_back(std::array<int, 4>{x, y, 0 ,0});
				spawn_point_[0] = x;
				spawn_point_[1] = y;
			}
		}
		return true;
}

void Level::Render(Player *player)
{
	if (exit_point_[0] > player->x_ - radius_ && exit_point_[0] < player->x_ + radius_ &&
		exit_point_[1] > player->y_ - radius_ && exit_point_[1] < player->y_ + radius_)
		al_draw_bitmap_region(image_, 256, 192, 64, 64, exit_point_[0] - 32, exit_point_[1] - 32, 0);

	for (auto &element : floor_tiles_)
	{
		if (element[0] > player->x_ - radius_ && element[0] < player->x_ + radius_ &&
			element[1] > player->y_ - radius_ && element[1] < player->y_ + radius_)
			al_draw_bitmap_region(image_, element[2], element[3], 64, 64, element[0] - 32, element[1] - 32, 0);
	}

	//Draw(player, floor_tiles_, 0, 0);
	Draw(player, vertical_walls_, 192, 64);
	Draw(player, horizontal_walls_, 256, 64);
	Draw(player, top_left_corners_, 320, 64);
	Draw(player, top_right_corners_, 0, 128);
	Draw(player, bot_right_corners_, 64, 128);
	Draw(player, bot_left_corners_, 128, 128);
	Draw(player, bot_t_shapes_, 192, 128);
	Draw(player, top_t_shapes_, 256, 128);
	Draw(player, right_t_shapes_, 320, 128);
	Draw(player, left_t_shapes_, 0, 192);
	Draw(player, bot_ends_, 64, 192);
	Draw(player, top_ends_, 128, 192);
	Draw(player, columns_, 192, 192);
}

inline void Level::Draw(Player *player, std::vector<std::array<int, 2>> map_elements, int region_x, int region_y)
{
	for (auto &element : map_elements)
	{
		if (element[0] > player->x_ - radius_ && element[0] < player->x_ + radius_ &&
			element[1] > player->y_ - radius_ && element[1] < player->y_ + radius_)
			al_draw_bitmap_region(image_, region_x, region_y, 64, 64, element[0] - 32, element[1] - 32, 0);
	}
}
