#include "Obstacle.h"

Obstacle::Obstacle(int x, int y, int hb_x, int hb_y)
{
	id_ = OBSTACLE;
	x_ = x;
	y_ = y;
	hb_x_ = hb_x;
	hb_y_ = hb_y;
	left_ = x_ - hb_x_;
	right_ = x_ + hb_x_;
	top_ = y_ - hb_y_;
	bot_ = y_ + hb_y_;

	//top left
	vertices_[0] = left_;
	vertices_[1] = top_;

	//bot left
	vertices_[2] = left_;
	vertices_[3] = bot_;

	//bot right
	vertices_[4] = right_;
	vertices_[5] = bot_;

	//top right
	vertices_[6] = right_;
	vertices_[7] = top_;

	//vertices_[0].x = left_;
	//vertices_[0].y = top_;

	//vertices_[1].x = right_;
	//vertices_[1].y = top_;

	//vertices_[2].x = left_;
	//vertices_[2].y = bot_;

	//vertices_[3].x = right_;
	//vertices_[3].y = bot_;

}

void Obstacle::Render()
{
	al_draw_filled_rectangle(x_ - hb_x_, y_ - hb_y_,
		x_ + hb_x_, y_ + hb_y_, al_map_rgb(255, 0, 255));
}