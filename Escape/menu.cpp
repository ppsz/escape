#include "menu.h"
#include "supervisor.h"

Menu::Menu(int * game_state, ALLEGRO_DISPLAY_MODE & disp_data, ALLEGRO_FONT * font)
{
	position_ = MP_START;
	menu_state_ = MS_MAIN_MENU;
	game_state_ = game_state;
	font_ = font;
	x_ = disp_data.width;
	y_ = disp_data.height;
	x_ /= 2;
	y_ /= 2;
	interval_ = disp_data.height / 9;
}

void Menu::Render()
{
	//al_draw_bitmap(image_, 0, 0, 0);
	al_draw_filled_rectangle(0, 0, x_ * 2, y_ * 2, al_map_rgb(0, 0, 0));

	if (*game_state_ == WIN)
	{
		al_draw_text(font_, al_map_rgb(255, 255, 255), x_, y_, ALLEGRO_ALIGN_CENTER, "You've finished the game! Press enter to continue");
	}
	else if (*game_state_ == GAME_OVER)
	{
		al_draw_text(font_, al_map_rgb(255, 255, 255), x_, y_, ALLEGRO_ALIGN_CENTER, "Game Over! Press enter to continue");
	}
	else
	{
		if (menu_state_ == MS_MAIN_MENU)
		{
			if (*game_state_ == MAIN_MENU)
				al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_*2, ALLEGRO_ALIGN_CENTRE, "New Game");
			else
				al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 2, ALLEGRO_ALIGN_CENTRE, "Continue");
			al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 4, ALLEGRO_ALIGN_CENTRE, "Load Game");
			if (*game_state_ != MAIN_MENU)
				al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 6, ALLEGRO_ALIGN_CENTRE, "Save Game");
			if (*game_state_ == MAIN_MENU)
				al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 8, ALLEGRO_ALIGN_CENTRE, "Exit");
			else
				al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 8, ALLEGRO_ALIGN_CENTRE, "Exit to menu");
		}
		else if (menu_state_ == MS_LOAD || menu_state_ == MS_SAVE)
		{
			al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 2, ALLEGRO_ALIGN_CENTRE, "Savefile 1");
			al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 4, ALLEGRO_ALIGN_CENTRE, "Savefile 2");
			al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 6, ALLEGRO_ALIGN_CENTRE, "Savefile 3");
			al_draw_text(font_, al_map_rgb(255, 255, 255), x_, interval_ * 8, ALLEGRO_ALIGN_CENTRE, "Back");
		}
		//al_draw_rectangle(x_ - 100, ((position_ + 1) * 100) + 45, x_ + 100, ((position_ + 2) * interval_) - 27, al_map_rgb(255, 255, 255), 5);
		al_draw_rectangle(x_ - 100, ((position_ + 1) * (interval_ * 2)) + 45, x_ + 100, ((position_ + 1) * (interval_ * 2)) - 27, al_map_rgb(255, 255, 255), 5);
	}
}

void Menu::Update()
{
}

void Menu::Navigate(int key, float mouse_y_position)
{
	int max_position = 3;
	//if (menu_state_ == MS_MAIN_MENU)
	//	max_position = 3;
	//else if (menu_state_ == MS_LOAD || menu_state_ == MS_SAVE)
	//	max_position = 3;
	//position_ = std::floor(mouse_y_position - 100) / 100;
	if (counter_++ == 0)
	{
		switch (key)
		{
		case UP:
			//--position_;
			if (--position_ < 0) position_ = max_position;
			if (*game_state_ == MAIN_MENU && menu_state_ == MS_MAIN_MENU && position_ == MP_SAVE) --position_;
			break;
		case DOWN:
			if (++position_ > max_position) position_ = 0;
			if (*game_state_ == MAIN_MENU && menu_state_ == MS_MAIN_MENU && position_ == MP_SAVE) ++position_;
			break;
		}
	}
	if (counter_ > 10) counter_ = 0;
}

void Menu::Select(Supervisor *supervisor)
{
	if (counter_++ == 0)
	{
		if (menu_state_ == MS_MAIN_MENU)
		{
			switch (position_)
			{
			case MP_START:
				position_ = 0;
				supervisor->ChangeState(PLAY);
				break;
			case MP_LOAD:
				position_ = 0;
				menu_state_ = MS_LOAD;
				break;
			case MP_SAVE:
				position_ = 0;
				menu_state_ = MS_SAVE;
				break;
			case MP_EXIT:
				if (*game_state_ == MAIN_MENU)
					supervisor->QuitGame();
				else
					supervisor->ChangeState(MAIN_MENU);
				break;
			}
		}
		else if (menu_state_ == MS_LOAD)
		{
			if (position_ != 3)
			{
				supervisor->LoadGame(position_ + 1);
				position_ = 3;
				al_draw_filled_rectangle(0, 0, x_ * 2, y_ * 2, al_map_rgb(0, 0, 0));
				al_draw_text(font_, al_map_rgb(255, 255, 255), x_, y_, ALLEGRO_ALIGN_CENTRE, "Loading game...");
				al_flip_display();
				al_rest(0.25);
			}
			position_ = 0;
			menu_state_ = MS_MAIN_MENU;
		}
		else if (menu_state_ == MS_SAVE)
		{
			if (position_ != 3)
			{
				if (supervisor->SaveGame(position_ + 1))
				{
					al_draw_filled_rectangle(0, 0, x_ * 2, y_ * 2, al_map_rgb(0, 0, 0));
					al_draw_text(font_, al_map_rgb(255, 255, 255), x_, y_, ALLEGRO_ALIGN_CENTRE, "Game saved succesfully");
					al_flip_display();
					al_rest(0.25);
				}
				else
				{
					al_draw_filled_rectangle(0, 0, x_ * 2, y_ * 2, al_map_rgb(0, 0, 0));
					al_draw_text(font_, al_map_rgb(255, 255, 255), x_, y_, ALLEGRO_ALIGN_CENTRE, "Error saving game file");
					al_flip_display();
					al_rest(0.75);
				}
			}
			position_ = 0;
			menu_state_ = MS_MAIN_MENU;
		}
	}
	if (counter_ > 10) counter_ = 0;
}
