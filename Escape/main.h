#pragma once
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <iostream>
#include <random>
#include <cmath>
#include <vector>
#include <array>
#include <atomic>
#include <fstream>
#include <string>

enum OBJECT_ID { PLAYER, ENEMY, PROJECTILE, OBSTACLE };
enum GAME_STATE { MAIN_MENU, PLAY, GAME_OVER, PAUSE, WIN };
enum KEYS { UP, DOWN, LEFT, RIGHT, CONFIRM, ESC, MOUSE };

const float PI = 3.141592;

inline float DegToRad(float degree)
{
	return (degree * PI) / 180;
}
inline float RadToDeg(float radian)
{
	return (radian * 180) / PI;
}
