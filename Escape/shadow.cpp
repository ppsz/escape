#include "Shadow.h"
#include "Player.h"
#include "Obstacle.h"

Shadow::Shadow(ALLEGRO_DISPLAY_MODE &disp_data)
{
	w_ = disp_data.width / 2;
	h_ = disp_data.height / 2;
	radius_ = (w_ < h_ ? w_ : h_);
	big_radius_ = radius_*1000;
}

Shadow::~Shadow()

{
}

void Shadow::Render(Player *player, std::vector<Obstacle> &obstacles)
{
	for (auto &obstacle : obstacles)
	{
		if (obstacle.x_ > player->x_ - radius_ && obstacle.x_ < player->x_ + radius_ &&
			obstacle.y_ > player->y_ - radius_ && obstacle.y_ < player->y_ + radius_)
		{
			//vertex 1,2,3
			if (player->x_ > obstacle.left_ && player->x_ < obstacle.right_)
			{
				if (player->y_ < obstacle.y_)
				{
					vertices_[0] = obstacle.vertices_[6];
					vertices_[1] = obstacle.vertices_[7];

					vertices_[2] = obstacle.x_;
					vertices_[3] = obstacle.top_;

					vertices_[4] = obstacle.vertices_[0];
					vertices_[5] = obstacle.vertices_[1];
				}
				else
				{
					vertices_[0] = obstacle.vertices_[2];
					vertices_[1] = obstacle.vertices_[3];

					vertices_[2] = obstacle.x_;
					vertices_[3] = obstacle.bot_;

					vertices_[4] = obstacle.vertices_[4];
					vertices_[5] = obstacle.vertices_[5];
				}
			}
			else if (player->y_ > obstacle.top_ && player->y_ < obstacle.bot_)
			{
				if (player->x_ < obstacle.x_)
				{
					vertices_[0] = obstacle.vertices_[0];
					vertices_[1] = obstacle.vertices_[1];

					vertices_[2] = obstacle.left_;
					vertices_[3] = obstacle.y_;

					vertices_[4] = obstacle.vertices_[2];
					vertices_[5] = obstacle.vertices_[3];
				}
				else
				{
					vertices_[0] = obstacle.vertices_[4];
					vertices_[1] = obstacle.vertices_[5];

					vertices_[2] = obstacle.right_;
					vertices_[3] = obstacle.y_;

					vertices_[4] = obstacle.vertices_[6];
					vertices_[5] = obstacle.vertices_[7];
				}
			}
			else if (player->x_ < obstacle.x_)
			{
				if (player->y_ < obstacle.y_)
				{
					vertices_[0] = obstacle.vertices_[6];
					vertices_[1] = obstacle.vertices_[7];

					vertices_[2] = obstacle.vertices_[0];
					vertices_[3] = obstacle.vertices_[1];

					vertices_[4] = obstacle.vertices_[2];
					vertices_[5] = obstacle.vertices_[3];
				}
				else
				{
					vertices_[0] = obstacle.vertices_[0];
					vertices_[1] = obstacle.vertices_[1];

					vertices_[2] = obstacle.vertices_[2];
					vertices_[3] = obstacle.vertices_[3];

					vertices_[4] = obstacle.vertices_[4];
					vertices_[5] = obstacle.vertices_[5];
				}
			}
			else
			{
				if (player->y_ < obstacle.y_)
				{
					vertices_[0] = obstacle.vertices_[4];
					vertices_[1] = obstacle.vertices_[5];

					vertices_[2] = obstacle.vertices_[6];
					vertices_[3] = obstacle.vertices_[7];

					vertices_[4] = obstacle.vertices_[0];
					vertices_[5] = obstacle.vertices_[1];
				}
				else
				{
					vertices_[0] = obstacle.vertices_[2];
					vertices_[1] = obstacle.vertices_[3];

					vertices_[2] = obstacle.vertices_[4];
					vertices_[3] = obstacle.vertices_[5];

					vertices_[4] = obstacle.vertices_[6];
					vertices_[5] = obstacle.vertices_[7];
				}
			}
			//float a, b;
			float deltaX, deltaY, degree;

			//a = (vertices_[5] - player->y_) / (vertices_[4] - player->x_);
			//b = player->y_ - a*player->x_;

			//vertex 4
			deltaX = (player->x_ - vertices_[4]);
			deltaY = (player->y_ - vertices_[5]);
			degree = std::atan2(deltaY, deltaX);
			vertices_[6] = vertices_[4] - (big_radius_ * cos(degree));
			vertices_[7] = vertices_[5] - (big_radius_ * sin(degree));

			//vertex 5
			deltaX = (player->x_ - vertices_[2]);
			deltaY = (player->y_ - vertices_[3]);
			degree = std::atan2(deltaY, deltaX);
			vertices_[8] = vertices_[2] - (big_radius_ * cos(degree));
			vertices_[9] = vertices_[3] - (big_radius_ * sin(degree));

			//vertex 6
			deltaX = (player->x_ - vertices_[0]);
			deltaY = (player->y_ - vertices_[1]);
			degree = std::atan2(deltaY, deltaX);
			vertices_[10] = vertices_[0] - (big_radius_ * cos(degree));
			vertices_[11] = vertices_[1] - (big_radius_ * sin(degree));

			//vertex 7
			vertices_[12] = vertices_[0];
			vertices_[13] = vertices_[1];

			al_draw_filled_polygon(&vertices_[0], 7, al_map_rgb(0, 0, 0));
		}
	}

	//vertices_[0].x = player->x_;
	//vertices_[0].y = player->y_;
	//vertices_[0].z = 0;
	//vertices_[0].color = al_map_rgba(255,255, 255, 255);

	//float j = 0;
	//for (int i = 1; i <= number_of_rays_; i++, j += iterator_)
	//{
	//	vertices_[i].x = radius_ * std::cos(DegToRad(j)) + player->x_;
	//	vertices_[i].y = radius_ * std::sin(DegToRad(j)) + player->y_;
	//	vertices_[i].z = 0;
	//	vertices_[i].color = al_map_rgba(255, 255, 255, 255);
	//	//al_draw_line(vertices_[0].x, vertices_[0].y, vertices_[i].x, vertices_[i].y, al_map_rgb(255, 255, 255), 1);
	//	for (auto &obstacle : obstacles)
	//	{
	//		if (player->x_ + w_ > obstacle.left_ &&
	//			player->x_ - w_ < obstacle.right_ &&
	//			player->y_ + w_ > obstacle.top_ &&
	//			player->y_ - w_ < obstacle.bot_)
	//		{
	//			LiangBarsky(player, obstacle, vertices_[i]);
	//		}
	//	}
	//}
	//vertices_[number_of_rays_ + 1] = vertices_[1];
	//al_draw_prim(vertices_, NULL, NULL, 0, number_of_rays_ + 2, ALLEGRO_PRIM_TRIANGLE_FAN);
}

bool Shadow::LiangBarsky(Player *player, Obstacle &obstacle, ALLEGRO_VERTEX &vertex)
{
	float vx = vertex.x - player->x_;
	float vy = vertex.y - player->y_;
	std::array<float, 4> p = { -vx, vx, -vy, vy };
	std::array<float, 4> q = {
	   player->x_ - (obstacle.left_),
	   (obstacle.right_) - player->x_,
	   player->y_ - (obstacle.top_),
	   (obstacle.bot_) - player->y_
	};
	float u1 = 0.0;
	float u2 = 1.0;

	for (int k = 0; k < 4; ++k)
	{
		if (p[k] == 0)
		{
			if (q[k] < 0.0)
			{
				return false;
			}
		}
		else
		{
			float t = q[k] / p[k];
			if (p[k] < 0.0 && u1 < t)
				u1 = t;
			else if (p[k] > 0.0 && u2 > t)
				u2 = t;
		}
	}

	if (u1 > u2 || u1 > 1.0 || u1 < 0.0)
	{
		return false;
	}

	vertex.x = (player->x_ + u1*(vertex.x - player->x_));
	vertex.y = (player->y_ + u1*(vertex.y - player->y_));

	return true;
}