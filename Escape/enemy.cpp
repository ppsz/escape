#include "enemy.h"
#include "obstacle.h"
#include "player.h"

ALLEGRO_BITMAP *Enemy::image_;
ALLEGRO_SAMPLE *Enemy::sample_1_;
ALLEGRO_SAMPLE *Enemy::sample_2_;
std::array<ALLEGRO_SAMPLE*, 12> Enemy::zombie_samples_;

Enemy::Enemy()
{
	static std::atomic<int> unique_id = 0;
	id_ = ENEMY;
	unique_id_ = unique_id++;
	state_ = AI_INACTIVE;

	degree_ = 0;
	speed_ = 2;
	rotation_speed_ = 5;
	sound_volume_ = 1.0;

	health_ = 100;
	damage_ = 10;
	hb_x_ = 15;
	hb_y_ = 15;

	max_frame_ = 4;
	cur_frame_ = 1;
	cur_animation_ = 0;

	frame_count_ = 0;
	frame_delay_ = 15;
	frame_width_ = 64;
	frame_height_ = 64;

	if (image_ == NULL)
		image_ = al_load_bitmap("data\\sprites\\zombie.png");

	if (sample_1_ == NULL)
		sample_1_ = al_load_sample("data\\sounds\\footstep_3.wav");

	if (sample_2_ == NULL)
		sample_2_ = al_load_sample("data\\sounds\\footstep_4.wav");

	footstep_1_ = al_create_sample_instance(sample_1_);
	footstep_2_ = al_create_sample_instance(sample_2_);
	for (int i = 0; i < 12; ++i)
	{
		std::string dir = "data\\sounds\\zombie_" + std::to_string(i+1) + ".wav";
		if(zombie_samples_[i] == NULL)
			zombie_samples_[i] = al_load_sample(dir.c_str());
	}

	al_attach_sample_instance_to_mixer(footstep_1_, al_get_default_mixer());
	al_attach_sample_instance_to_mixer(footstep_2_, al_get_default_mixer());
}

Enemy::~Enemy()
{
}

void Enemy::Collide(int &id, float &x, float &y, int &hb_x, int &hb_y, int unique_id)
{
	if (id == ENEMY && this->unique_id_ == unique_id)
		return;
	if (id == OBSTACLE && state_ == AI_SPAWN)
		Despawn();

	// Minkowski sum
	float w = 0.5 * (2 * hb_x + 2 * hb_x_);
	float h = 0.5 * (2 * hb_y + 2 * hb_y_);
	float dx = x - x_;
	float dy = y - y_;
	float wy = w * dy;
	float hx = h * dx;

	if (wy > hx)
	{
		if (wy > -hx)
		{
			// collision at the top 
			y_ = y - hb_y - hb_y_;
		}
		else
		{
			// on the right 
			x_ = x + hb_x + hb_x_;
		}
	}
	else
	{
		if (wy > -hx)
		{
			// on the left 
			x_ = x - hb_x - hb_x_;
		}
		else
		{
			// at the bottom 
			y_ = y + hb_y + hb_y_;
		}
	}
}

void Enemy::Update(Player *player, int &radius)
{
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_int_distribution<int> chance(0, 500);

	if (chance(mt) == 0)
	{
		std::uniform_int_distribution<int> dist(0, 11);
		al_play_sample(zombie_samples_[dist(mt)], sound_volume_, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
	}
		

	switch (state_)
	{
	case AI_IDLE:
	{
		degree_ = DegToRad(random_degree_);
		x_ -= (speed_ * cos(degree_));
		y_ -= (speed_ * sin(degree_));

		if ((++frame_count_ >= frame_delay_))
		{
			cur_frame_++;
			if (cur_frame_ >= max_frame_)
				cur_frame_ = 0;
			frame_count_ = 0;
		}
		break;
	}
	case AI_SPAWN:
	{
		if ((++frame_count_ >= frame_delay_ * 2))
		{
			cur_frame_++;
			if (cur_frame_ >= max_frame_)
			{
				cur_frame_ = 0;
				Stop();
			}
			frame_count_ = 0;
		}
		break;
	}
	case AI_CHASE:
	{
		float deltaX = (x_ - player->x_);
		float deltaY = (y_ - player->y_);
		degree_ = std::atan2(deltaY, deltaX);

		x_ -= (speed_ * cos(degree_));
		y_ -= (speed_ * sin(degree_));

		if ((++frame_count_ >= frame_delay_))
		{
			cur_frame_++;
			if (cur_frame_ >= max_frame_)
				cur_frame_ = 0;
			frame_count_ = 0;
		}
		break;
	}
	case AI_ATTACK:
	{
		float deltaX = (x_ - player->x_);
		float deltaY = (y_ - player->y_);
		degree_ = std::atan2(deltaY, deltaX);

		if ((++frame_count_ >= frame_delay_))
		{
			cur_frame_++;
			if (cur_frame_ >= max_frame_)
			{
				cur_frame_ = 0;
				Move();
			}
			frame_count_ = 0;
		}
		break;
	}
	default:
		break;
	}

	if (state_ == AI_IDLE || state_ == AI_CHASE)
	{
		sound_volume_ = 0.0;
		float distance = sqrt(pow(x_ - player->x_, 2) + pow(y_ - player->y_, 2));
		if (distance != 0)
			sound_volume_ = static_cast<float>(distance) / static_cast<float>(radius * 2);
		sound_volume_ = 1.0 - sound_volume_;

		al_set_sample_instance_gain(footstep_1_, sound_volume_);
		al_set_sample_instance_gain(footstep_2_, sound_volume_);
		if (cur_frame_ == 0)
			al_play_sample_instance(footstep_1_);
		else if (cur_frame_ == 2)
			al_play_sample_instance(footstep_2_);
	}

}

void Enemy::Render()
{
	if (state_ != AI_INACTIVE)
	{
		al_draw_tinted_scaled_rotated_bitmap_region(
			image_,
			cur_frame_ * frame_width_,
			cur_animation_ * frame_height_,
			64, 64,
			al_map_rgb(255, 255, 255),
			32, 32,
			std::floorf(x_), std::floorf(y_),
			1, 1,
			degree_,
			0);

		//Hitbox
		/*al_draw_filled_rectangle(x_ - hb_x_, y_ - hb_y_,
								 x_ + hb_x_, y_ + hb_y_, al_map_rgba(255, 0, 255, 100));*/
	}
}

void Enemy::Spawn(float x, float y, int width, int height)
{
	static int max_w = width / 64;
	static int max_h = height / 64;
	if (state_ == AI_INACTIVE)
	{
		std::random_device rd;
		static std::mt19937 mt(rd());
		static std::uniform_int_distribution<int> w(0, max_w);
		static std::uniform_int_distribution<int> h(0, max_h);
		x_ = x + w(mt) * 64;
		y_ = y + h(mt) * 64;

		cur_frame_ = 2;
		max_frame_ = 4;
		cur_animation_ = AN_STOP;
		frame_count_ = 0;

		state_ = AI_SPAWN;
	}
}

void Enemy::Despawn()
{
	if (state_ != AI_INACTIVE)
	{
		degree_ = 0;
		health_ = 100;
		cur_frame_ = 1;
		cur_animation_ = AN_STOP;
		frame_count_ = 0;
		state_ = AI_INACTIVE;
	}
}

void Enemy::Move()
{
	if (state_ != AI_CHASE)
	{
		cur_animation_ = AN_WALK;
		cur_frame_ = 1;
		max_frame_ = 4;
		state_ = AI_CHASE;
	}
}

void Enemy::Stop()
{
	if (state_ != AI_IDLE)
	{
		cur_animation_ = AN_WALK;
		cur_frame_ = 1;
		max_frame_ = 4;
		state_ = AI_IDLE;
	}
}

void Enemy::Chase()
{
	if (state_ == AI_IDLE)
	{
		cur_animation_ = AN_WALK;
		cur_frame_ = 1;
		max_frame_ = 4;
		state_ = AI_CHASE;
	}
}

void Enemy::Idle()
{
	if (state_ != AI_IDLE && state_ != AI_SPAWN)
	{
		static std::random_device rd;
		static std::mt19937 mt(rd());
		std::uniform_int_distribution<int> dist(0, 360);

		random_degree_ = dist(mt);
		cur_animation_ = AN_WALK;
		cur_frame_ = 1;
		max_frame_ = 4;
		state_ = AI_IDLE;
	}
}

void Enemy::Attack(Player *player)
{
	if (state_ != AI_ATTACK && state_ != AI_SPAWN)
	{
		player->health_ -= damage_;
		cur_animation_ = AN_ATTACK;
		cur_frame_ = 0;
		max_frame_ = 2;
		state_ = AI_ATTACK;
	}
}

