#include "supervisor.h"
#include "player.h"
#include "enemy.h"
#include "obstacle.h"
#include "level.h"
#include "menu.h"

Supervisor::Supervisor(Player *player, Shadow *shadow, Menu *menu, Level *level, std::vector<Enemy> &enemies, int &number_of_enemies, int &game_state, bool &done, ALLEGRO_DISPLAY_MODE &disp_data)
{
	player_ = player;
	shadow_ = shadow;
	menu_ = menu;
	level_ = level;
	enemies_ = &enemies;
	number_of_enemies_ = number_of_enemies;
	game_state_ = &game_state;
	done_ = &done;

	full_w_ = disp_data.width;
	full_h_ = disp_data.height;
	w_ = disp_data.width / 2;
	h_ = disp_data.height / 2;
	radius_ = (w_ < h_ ? w_ : h_);

	current_track_ = 1;

	game_over_sample_ = al_load_sample("data\\sounds\\game_over.wav");
	win_sample_ = al_load_sample("data\\sounds\\win.ogg");
	damage_sample_ = al_load_sample("data\\sounds\\damage.wav");

	damage_sample_instance_ = al_create_sample_instance(damage_sample_);
	al_attach_sample_instance_to_mixer(damage_sample_instance_, al_get_default_mixer());

	for (int i = 0; i < 3; ++i)
	{
		std::string dir = "data\\tracks\\track_" + std::to_string(i) + ".ogg";
		track_samples_[i] = al_load_sample(dir.c_str());
	}

	for (int i = 0; i < 3; ++i)
	{
		track_sample_instances_[i] = al_create_sample_instance(track_samples_[i]);
		al_attach_sample_instance_to_mixer(track_sample_instances_[i], al_get_default_mixer());
		al_set_sample_instance_gain(track_sample_instances_[i], 0.7);
		al_set_sample_instance_playmode(track_sample_instances_[i], ALLEGRO_PLAYMODE_LOOP);
	}
}

void Supervisor::DetectColissions()
{
	if (level_->exit_point_[0] + level_->obstacle_hb_ > player_->x_ - player_->hb_x_ &&
		level_->exit_point_[0] - level_->obstacle_hb_ < player_->x_ + player_->hb_x_ &&
		level_->exit_point_[1] + level_->obstacle_hb_ > player_->y_ - player_->hb_y_ &&
		level_->exit_point_[1] - level_->obstacle_hb_ < player_->y_ + player_->hb_y_)
	{
		al_draw_filled_rectangle(0, 0, full_w_, full_h_, al_map_rgb(0, 0, 0));
		al_flip_display();
		al_rest(0.25);
		
		level_->cur_level_++;

		if (!level_->Load())
			ChangeState(WIN);
			
		player_->Spawn(level_->spawn_point_);
	}

	for (auto &enemy : *enemies_)
	{
		float distance = sqrt(pow(enemy.x_ - player_->x_, 2) + pow(enemy.y_ - player_->y_, 2));
		enemy.Spawn(player_->x_ - w_, player_->y_ - h_, full_w_, full_h_);
		if (distance < radius_ * 2)
		{
			if (distance < radius_)
			{
				enemy.Chase();
				if (enemy.x_ + enemy.hb_x_ > player_->x_ - player_->hb_x_ &&
					enemy.x_ - enemy.hb_x_ < player_->x_ + player_->hb_x_ &&
					enemy.y_ + enemy.hb_y_ > player_->y_ - player_->hb_y_ &&
					enemy.y_ - enemy.hb_y_ < player_->y_ + player_->hb_y_)
				{
					player_->Collide(enemy.id_, enemy.x_, enemy.y_, enemy.hb_x_, enemy.hb_y_);
					enemy.Attack(player_);
					al_play_sample_instance(damage_sample_instance_);
					if (player_->health_ <= 0) ChangeState(GAME_OVER);
				}
				for (auto &other_enemy : *enemies_)
				{
					if (enemy.x_ + enemy.hb_x_ > other_enemy.x_ - other_enemy.hb_x_ &&
						enemy.x_ - enemy.hb_x_ < other_enemy.x_ + other_enemy.hb_x_ &&
						enemy.y_ + enemy.hb_y_ > other_enemy.y_ - other_enemy.hb_y_ &&
						enemy.y_ - enemy.hb_y_ < other_enemy.y_ + other_enemy.hb_y_)
					{
						enemy.Collide(other_enemy.id_, other_enemy.x_, other_enemy.y_, other_enemy.hb_x_, other_enemy.hb_y_, other_enemy.unique_id_);
					}
				}
			}
			else
			{
				enemy.Idle();
			}
		}
		else
		{
			enemy.Despawn();
		}
	}
	for (auto &obstacle : level_->obstacles_)
	{
		if (player_->x_ + player_->hb_x_ > obstacle.left_ &&
			player_->x_ - player_->hb_x_ < obstacle.right_&&
			player_->y_ + player_->hb_y_ > obstacle.top_ &&
			player_->y_ - player_->hb_y_ < obstacle.bot_ )
		{
			player_->Collide(obstacle.id_, obstacle.x_, obstacle.y_, obstacle.hb_x_, obstacle.hb_y_);
		}
		for (auto &enemy : *enemies_)
		{
			if (enemy.x_ + enemy.hb_x_ > obstacle.left_ &&
				enemy.x_ - enemy.hb_x_ < obstacle.right_&&
				enemy.y_ + enemy.hb_y_ > obstacle.top_ &&
				enemy.y_ - enemy.hb_y_ < obstacle.bot_)
			{
				enemy.Collide(obstacle.id_, obstacle.x_, obstacle.y_, obstacle.hb_x_, obstacle.hb_y_);
			}
		}
	}
}

void Supervisor::ChangeState(int new_state, bool load)
{
	switch (new_state)
	{
	case MAIN_MENU:
		for (auto &track : track_sample_instances_)
			al_stop_sample_instance(track);
		al_play_sample_instance(track_sample_instances_[0]);
		level_->cur_level_ = 1;
		*game_state_ = MAIN_MENU;
		break;
	case PLAY:
		if (*game_state_ == MAIN_MENU)
		{
			al_stop_sample_instance(track_sample_instances_[0]);
			al_play_sample_instance(track_sample_instances_[current_track_]);
			level_->Load();
			if(!load) player_->Respawn(level_->spawn_point_);
			enemies_->resize(number_of_enemies_);
			if (!load)
			{
				for (auto &enemy : *enemies_)
				{
					enemy.x_ = -1024;
					enemy.y_ = -1024;
				}
			}
		}
		*game_state_ = PLAY;
		break;
	case GAME_OVER:
		for (auto &track : track_sample_instances_)
			al_stop_sample_instance(track);
		al_play_sample(game_over_sample_, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
		level_->cur_level_ = 1;
		*game_state_ = GAME_OVER;
		break;
	case PAUSE:
		*game_state_ = PAUSE;
		break;
	case WIN:
		for (auto &track : track_sample_instances_)
			al_stop_sample_instance(track);
		al_play_sample(win_sample_, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
		level_->cur_level_ = 1;
		*game_state_ = WIN;
		break;
	default:
		break;
	}
}

void Supervisor::LoadGame(int file_number)
{
	std::ifstream save_file("data\\saves\\" + std::to_string(file_number) + ".save");
	if (save_file.is_open())
	{
		save_file >> player_->x_;
		save_file >> player_->y_;
		save_file >> player_->degree_;
		save_file >> player_->health_;
		save_file >> level_->cur_level_;
		save_file >> number_of_enemies_;
		level_->Load();
		enemies_->clear();
		enemies_->resize(number_of_enemies_);
		for (auto &enemy : *enemies_)
		{
			save_file >> enemy.state_;
			save_file >> enemy.x_;
			save_file >> enemy.y_;
		}
	}
	save_file.close();
	ChangeState(PLAY, true);
}

bool Supervisor::SaveGame(int file_number)
{
	std::ofstream save_file("data\\saves\\" + std::to_string(file_number) + ".save", std::ofstream::trunc);
	if (save_file.is_open())
	{
		save_file << player_->x_ << " " << player_->y_ << " " << player_->degree_ << " " << player_->health_ << std::endl;
		save_file << level_->cur_level_ << std::endl;
		save_file << number_of_enemies_ << std::endl;
		for (auto &enemy : *enemies_)
			save_file << enemy.state_ << " " << enemy.x_ << " " << enemy.y_ << std::endl;
		save_file.close();
		return true;
	}
	return false;
}

void Supervisor::QuitGame()
{
	*done_ = true;
}
