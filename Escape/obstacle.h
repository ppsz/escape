#pragma once
#include "main.h"

struct Obstacle
{
	int id_;
	float x_;
	float y_;
	int collision_hb_;
	int hb_x_;
	int hb_y_;
	float left_;
	float right_;
	float top_;
	float bot_;

	std::array<float, 8> vertices_;

	Obstacle(int x, int y, int hb_x=32, int hb_y=32);
	void Render();
};

