#pragma once
#include "Main.h"

struct Player;
struct Obstacle;

struct Shadow
{
private:
	int w_;
	int h_;
	int radius_;
	int big_radius_;
	static const int number_of_rays_ = 1440;
	float iterator_ = 360.0 / number_of_rays_;
	std::array<float,14> vertices_;
	//ALLEGRO_VERTEX vertices_[number_of_rays_ + 2];

public:
	Shadow(ALLEGRO_DISPLAY_MODE &disp_data);
	~Shadow();
	void Render(Player *player, std::vector<Obstacle> &obstacles);
private:
	bool LiangBarsky(Player *player, Obstacle &obstacle, ALLEGRO_VERTEX &vertex);
};

