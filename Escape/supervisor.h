#pragma once
#include "Main.h"

struct Obstacle;
struct Enemy;
struct Player;
struct Level;
struct Menu;
struct Shadow;

struct Supervisor
{
	Player *player_;
	Shadow *shadow_;
	Menu *menu_;
	Level *level_;
	std::vector<Enemy> *enemies_;
	
	bool *done_;
	int *game_state_;
	int number_of_enemies_;
	int full_w_;
	int full_h_;
	int w_;
	int h_;
	int radius_;
	int current_track_;
	float player_x_;
	float player_y_;

	ALLEGRO_SAMPLE *game_over_sample_;
	ALLEGRO_SAMPLE *win_sample_;
	ALLEGRO_SAMPLE *damage_sample_;
	ALLEGRO_SAMPLE_INSTANCE *damage_sample_instance_;

	std::array<ALLEGRO_SAMPLE*, 3> track_samples_;
	std::array<ALLEGRO_SAMPLE_INSTANCE*, 3> track_sample_instances_;

	Supervisor(Player *player, Shadow *shadow, Menu *menu, Level *level, std::vector<Enemy> &enemies, int &number_of_enemies, int &game_state, bool &done, ALLEGRO_DISPLAY_MODE &disp_data);
	void DetectColissions();
	void ChangeState(int new_state, bool load = false);
	void LoadGame(int file_number);
	bool SaveGame(int file_number);
	void QuitGame();
};