#pragma once
#include "main.h"

struct Obstacle;
struct Player;

struct Level
{
	int w_;
	int h_;
	int radius_;
	int x_;
	int y_;
	int cur_level_;
	int map_square_size_;
	int obstacle_hb_;

	std::vector<Obstacle> obstacles_;
	std::vector<Obstacle> shadow_obstacles_;

	std::vector<std::array<int, 4>> floor_tiles_;
	std::vector<std::array<int, 2>> vertical_walls_;   // ||
	std::vector<std::array<int, 2>> horizontal_walls_; // ==
	std::vector<std::array<int, 2>> top_left_corners_;
	std::vector<std::array<int, 2>> top_right_corners_;
	std::vector<std::array<int, 2>> bot_right_corners_;
	std::vector<std::array<int, 2>> bot_left_corners_;
	std::vector<std::array<int, 2>> bot_t_shapes_;
	std::vector<std::array<int, 2>> top_t_shapes_;
	std::vector<std::array<int, 2>> right_t_shapes_;
	std::vector<std::array<int, 2>> left_t_shapes_;
	std::vector<std::array<int, 2>> bot_ends_;
	std::vector<std::array<int, 2>> left_ends_;
	std::vector<std::array<int, 2>> right_ends_;
	std::vector<std::array<int, 2>> top_ends_;
	std::vector<std::array<int, 2>> columns_;

	std::array<std::array<int, 2>, 9> lookup_table_ = { {{ 0,0 },{ 64,0 },{ 128,0 },{ 192,0 },{ 256,0 },{ 320,0 },{ 0,64 },{ 64,64 },{ 128,64 }} };
	std::array<std::array<int, 64>, 64> map_;
	std::array<float, 2> spawn_point_;
	std::array<float, 2> exit_point_;

	ALLEGRO_BITMAP *image_ = al_load_bitmap("data\\sprites\\level.png");

	Level(ALLEGRO_DISPLAY_MODE &disp_data);
	bool Load();
	void Render(Player *player);
	inline void Draw(Player *player, std::vector<std::array<int, 2>> map_elements, int region_x, int region_y);
};

