#include "Player.h"

Player::Player(std::array<float, 2> spawn_point)
{
	id_ = PLAYER;
	x_ = spawn_point[0];
	y_ = spawn_point[1];
	degree_ = 0;
	speed_ = 3;
	rotation_speed_ = 5;
	cur_speed_ = speed_;
	direction_ = MV_STOP;
	rotation_ = RT_STOP;

	health_ = 100;
	hb_x_ = 10;
	hb_y_ = 10;

	max_frame_ = 4;
	cur_frame_ = 1;
	weapon_ = 4;
	model_ = 0;

	frame_count_ = 0;
	frame_delay_ = 15;
	frame_width_ = 64;
	frame_height_ = 64;

	image_ = al_load_bitmap("data\\sprites\\player.png");
	
	sample_1_ = al_load_sample("data\\sounds\\footstep_1.wav");
	sample_2_ = al_load_sample("data\\sounds\\footstep_2.wav");
	footstep_1_ = al_create_sample_instance(sample_1_);
	footstep_2_ = al_create_sample_instance(sample_2_);

	al_attach_sample_instance_to_mixer(footstep_1_, al_get_default_mixer());
	al_attach_sample_instance_to_mixer(footstep_2_, al_get_default_mixer());

	al_set_sample_instance_gain(footstep_1_, 0.7);
	al_set_sample_instance_gain(footstep_2_, 0.7);
}

void Player::Respawn(std::array<float, 2> spawn_point)
{
	id_ = PLAYER;
	x_ = spawn_point[0];
	y_ = spawn_point[1];
	degree_ = 0;
	speed_ = 3;
	rotation_speed_ = 5;
	cur_speed_ = speed_;
	direction_ = MV_STOP;
	rotation_ = RT_STOP;

	health_ = 100;
	hb_x_ = 10;
	hb_y_ = 10;

	max_frame_ = 4;
	cur_frame_ = 1;
	weapon_ = 4;
	model_ = 0;

	frame_count_ = 0;
	frame_delay_ = 15;
	frame_width_ = 64;
	frame_height_ = 64;
}

void Player::Spawn(std::array<float, 2> spawn_point)
{
	x_ = spawn_point[0];
	y_ = spawn_point[1];
}

Player::~Player()
{
}

void Player::Collide(int &id, float &x, float &y, int &hb_x, int &hb_y)
{
	// Minkowski sum
	float w = 0.5 * (2 * hb_x + 2 * hb_x_);
	float h = 0.5 * (2 * hb_y + 2 * hb_y_);
	float dx = x - x_;
	float dy = y - y_;
	float wy = w * dy;
	float hx = h * dx;

	if (wy > hx)
	{
		if (wy > -hx)
		{
			// collision at the top 
			y_ = y - hb_y - hb_y_;
		}
		else
		{
			// on the right 
			x_ = x + hb_x + hb_x_;
		}
	}
	else
	{
		if (wy > -hx)
		{
			// on the left 
			x_ = x - hb_x - hb_x_;
		}
		else
		{
			// at the bottom 
			y_ = y + hb_y + hb_y_;
		}
	}
}

void Player::Update()
{
	if ((++frame_count_ >= frame_delay_))
	{
		cur_frame_ += direction_;
		if (cur_frame_ >= max_frame_)
			cur_frame_ = 0;
		else if (cur_frame_ <= 0)
			cur_frame_ = max_frame_ - 1;
		else if (direction_ == 0)
			cur_frame_ = 1;

		frame_count_ = 0;
	}

	if (cur_frame_ == 0)
		al_play_sample_instance(footstep_1_);
	else if (cur_frame_ == 2)
		al_play_sample_instance(footstep_2_);

	x_ += cur_speed_ * std::sin(DegToRad(degree_));
	y_ -= cur_speed_ * std::cos(DegToRad(degree_));
	if (direction_ == MV_BACKWARD)
		degree_ -= rotation_speed_ * rotation_;
	else
		degree_ += rotation_speed_ * rotation_;
}

void Player::Render()
{
	//Legs
	al_draw_tinted_scaled_rotated_bitmap_region(
		image_,
		cur_frame_ * frame_width_,
		model_ * frame_height_,
		64, 64,
		al_map_rgb(255, 255, 255),
		32, 32,
		std::floorf(x_), std::floorf(y_),
		1, 1,
		DegToRad(degree_),
		0);
	//Torso
	al_draw_tinted_scaled_rotated_bitmap_region(
		image_,
		weapon_ * frame_width_,
		model_ * frame_height_,
		64, 64,
		al_map_rgb(255, 255, 255),
		32, 32,
		std::floorf(x_), std::floorf(y_),
		1, 1,
		DegToRad(degree_),
		0);

	//Hitbox 

	/*al_draw_filled_rectangle(x_ - hb_x_, y_ - hb_y_,
							 x_ + hb_x_, y_ + hb_y_, al_map_rgba(255, 0, 255, 100));*/

}

void Player::StopMovement()
{
	cur_speed_ = MV_STOP;
	direction_ = MV_STOP;
}

void Player::StopRotation()
{
	rotation_ = RT_STOP;
}

void Player::MoveForward()
{
	cur_speed_ = speed_;
	direction_ = MV_FORWARD;
}

void Player::MoveBackward()
{
	cur_speed_ = -speed_ / 2;
	direction_ = MV_BACKWARD;
}

void Player::RotateLeft()
{
	rotation_ = RT_LEFT;
}

void Player::RotateRight()
{
	rotation_ = RT_RIGHT;
}
